/*
 * This file is part of NGlippy.
 *
 * NGlippy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NGlippy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NGlippy.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NGLIPPY_APPLICATION_H
#define NGLIPPY_APPLICATION_H

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define NGLIPPY_TYPE_APPLICATION (nglippy_application_get_type())
G_DECLARE_FINAL_TYPE(NglippyApplication, nglippy_application, NGLIPPY, APPLICATION, GtkApplication)

NglippyApplication *nglippy_application_new();

G_END_DECLS

#endif /* NGLIPPY_APPLICATION_H */
