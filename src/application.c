/*
 * This file is part of NGlippy.
 *
 * NGlippy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NGlippy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with NGlippy.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

struct _NglippyApplication {
  GtkApplication parent_instance;
};
G_DEFINE_TYPE (NglippyApplication, nglippy_application, GTK_TYPE_APPLICATION)

static void nglippy_application_init(NglippyApplication *app) {

}

static void nglippy_application_class_init(NglippyApplicationClass *class) {

}

NglippyApplication *nglippy_application_new() {
  return g_object_new(NGLIPPY_TYPE_APPLICATION, NULL);
}
